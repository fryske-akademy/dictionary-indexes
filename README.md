# README #

Project to extract dictionary index from a word file. Especially the [midslanderwurdboek](midslanderwurdboek) approache is pretty generic.

The general idea is to extract text from dictionary documents where every entry is in a paragraph, the lemma is bold and translations can be recognized by a certain style.
- first a document is converted to odt format
- then an xslt script conerts the paragraphs to text lines containing style markers
- an awk script converts the lines into xml index entries containing lammas in two languages
- finally an xquery script converts the xml into a textual index that can be imported into an office document. A series of sed commands performs some fine tuning and adds a utf-8 header.

The scripts are quite readable so you can adapt them to your own needs.