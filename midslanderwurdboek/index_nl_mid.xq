xquery version "3.0";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "text";

let $doc := doc("mllijst.xml")
return
    for $nl in $doc/index/entry/nl
    let $n := $nl
    group by $nl
    order by $nl collation 'http://saxon.sf.net/collation?ignore-case=yes'
    return ($nl, '  ',
        for $h in distinct-values($doc/index/entry/ml[../nl/text()[.=$n/text()]]/text())
        return ($h,'; ')
        , '&#13;&#10;')
