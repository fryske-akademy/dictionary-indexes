BEGIN {
    startund=" @U "
    startbold=" @B "
    endund=" U@ "
    endbold=" B@ "
    stomregex=" B@  @E r E@  @B "
    print "<index>"
}
{
    sub(stomregex,"r")
    print "<entry>"
    re = "^" startbold "([^@]+)" endbold ".*$"
    split(gensub(/'/,"","g",gensub(re,"\\1",1)),ls,"; ")
    lemma=gensub(/,|, |;|; /,"","g",ls[1])
    notnom=index($0," nom. ")==0
    if (notnom) lemma=tolower(lemma)
    print "<ml>" lemma "</ml>"
    patsplit($0,nls,startund "[^@]*" endund,seps)
    for (i in nls) {
        nl=substr(nls[i],5,length(nls[i])-8)
        if (notnom) nl=tolower(nl)
        print "<nl>" nl "</nl>"
    }
    print "</entry>"
}
END {print "</index>"}
