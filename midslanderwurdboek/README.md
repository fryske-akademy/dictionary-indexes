## midslanners

- open in libreoffice
- save as .odt
- unzip odt

```
java -Xmx4g -cp ~/saxon-he-11.3/saxon-he-11.3.jar net.sf.saxon.Transform -xsl:../fi_wurdboek/src/main/resources/convertUseStyleAndText.xslt -s:content.xml > midslanners.txt 2>logml.txt
gawk -f midslanners.awk < midslanners.txt  >mllijst.xml
java -Xmx4g -cp ~/saxon-he-11.3/saxon-he-11.3.jar net.sf.saxon.Query -q:index_nl_mid.xq |sed -f ../nabewerking.sed > index_ml.txt
```


## use the index ##

copy and adapt the index as needed into the dictionary word file
