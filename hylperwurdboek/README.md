# README #

Project to extract dictionary index from a word file, works for hylper document in this repo.
For more generic index building use [convertUseStyleAndText.xslt](https://bitbucket.org/fryske-akademy/fi_wurdboek/src/master/src/main/resources/convertUseStyleAndText.xslt) as basis.

## preparation ##

* Click the File tab > Options > Trust Center > Trust Center Settings > Privacy Option. In the Document-specific settings section, uncheck the Store random numbers.
* save file
* unzip file
* xmllint --format word/document.xml > formatted.xml

## collect ##

java -cp ~/saxonpe/saxon9pe.jar -Xmx2048m net.sf.saxon.Query -q:index.xq |xmllint --format - > lijst.xml

## build dutch and frisian index ##

java -cp ~/saxonpe/saxon9pe.jar -Xmx2048m net.sf.saxon.Query -q:index_nl.xq > nl_index.txt
java -cp ~/saxonpe/saxon9pe.jar -Xmx2048m net.sf.saxon.Query -q:index_fr.xq > fr_index.txt

## gather errors ##

java -cp ~/saxonpe/saxon9pe.jar -Xmx2048m net.sf.saxon.Query -q:index_err.xq > err_index.txt

## postprocess

```
sed -f nabewerking.sed
```

## use the index ##

copy and adapt the index as needed into the dictionary word file
