xquery version "3.0";

(:~
: User: eduard
: Date: 8-3-17
: Time: 8:32
: To change this template use File | Settings | File Templates.
:)
declare namespace w="http://schemas.openxmlformats.org/wordprocessingml/2006/main";
declare variable $wurd_regex as xs:string := '^.*?([A-Z][a-zA-Z, ûôúâêéëïöó()]*);.*?$';
declare variable $frOnly as xs:string := '&lt;Fr.&gt;';

declare function local:nl($rs) {
    let $w := local:nlword('',$rs)
    return distinct-values(tokenize($w,' '))
};

declare function local:mayHaveFr($t) {
    contains($t,';') or contains($t,$frOnly)
};
declare function local:hylperword($r) {
    if ($r/following-sibling::w:r[starts-with(w:rPr/w:rStyle/@w:val,'Style') and not(w:rPr/w:vanish)]) then
        concat($r/w:t/text(),local:hylperword($r/following-sibling::w:r[starts-with(w:rPr/w:rStyle/@w:val,'Style') and not(w:rPr/w:vanish)][1]))
    else
        $r/w:t/text()
};
declare function local:nlword($txt,$r) {
    if (trace($r/following-sibling::w:r[1][w:rPr/w:u],concat('nl-',$txt,'-',$r/following-sibling::w:r[1][w:rPr/w:u]/w:t/text()))) then
        local:nlword(concat($txt,$r/w:t/text()),$r/following-sibling::w:r[1])
    else
        if ($r/following-sibling::w:r[w:rPr/w:u]) then
            local:nlword(concat($txt,$r/w:t/text(),' '),$r/following-sibling::w:r[w:rPr/w:u][1])
        else
            concat($txt,$r/w:t/text())
};
declare function local:plakken($txt,$hyphenated) {
        if ($hyphenated/following-sibling::w:r[1][w:softHyphen]) then
            local:plakken(concat($txt,$hyphenated/w:t/text()),$hyphenated/following-sibling::w:r[1])
        else
            concat($txt,$hyphenated/w:t/text())
};
declare function local:afterlast($txt,$delim) {
        if (contains($txt,$delim)) then
            local:afterlast(substring-after($txt,$delim),$delim)
        else
            $txt
};
declare function local:frword($fw,$beforesemicolon) {
    for $r in $fw
    return
        if ($r/following-sibling::w:r[1][w:softHyphen]) then
            if ($beforesemicolon) then
                substring-before(local:plakken(local:afterlast($r/w:t/text(),'. '),$r/following-sibling::w:r[1]),';')
            else
                local:plakken(local:afterlast($r/w:t/text(),'. '),$r/following-sibling::w:r[1])
        else
            if ($beforesemicolon) then
                local:afterlast(substring-before($r/w:t/text(),';'),'. ')
            else
                local:afterlast(substring-after($r/w:t/text(),';'),'. ')
};
declare function local:frback($frparts) {
    for $fr in $frparts
    return 
        if ($fr/preceding-sibling::w:r[1][w:softHyphen]) then
            local:frback($fr/preceding-sibling::w:r[1])
        else
            if ($fr/preceding-sibling::w:r[1][w:t[contains(text(),'. ')]]) then
                $fr/preceding-sibling::w:r[1]
            else
                $fr
};
declare function local:fr($p) {
    for $r in $p/w:r[w:t[contains(text(),$frOnly)]] |
        local:frback($p/w:r[w:t[contains(text(),';') and not(contains(text(),'amp;'))]])
    let $frwords := local:frword($r,not($r[w:t[contains(text(),$frOnly)]]))
    return  
        let $w := distinct-values(tokenize(replace($frwords,$wurd_regex,'$1'),','))
        return $w
};

<index>{
let $doc := doc("formatted.xml")
return (
    for $p in $doc/w:document/w:body/w:p[w:r[not(w:rPr/w:vanish) and starts-with(w:rPr/w:rStyle/@w:val,'Style')]]
    where not(empty($p/w:r[string-length(normalize-space(w:t/text())) > 0 and not(w:rPr/w:vanish) and starts-with(w:rPr/w:rStyle/@w:val,'Style')]))
    return <entry>{(
        <hylp>{local:hylperword($p/w:r[string-length(normalize-space(w:t/text())) > 0 and not(w:rPr/w:vanish) and starts-with(w:rPr/w:rStyle/@w:val,'Style')][1])}</hylp>
        ,
        for $ws in local:nl($p/w:r[w:rPr/w:u][1])
        return <nl>{$ws}</nl>
                ,
        for $fr in local:fr($p[w:r/w:t[local:mayHaveFr(text())]])
        where matches($fr,'^[A-Z][a-zA-Z, ûôúâêéëïöó\(\)]*$')
        order by $fr collation 'http://fryske-akademy.nl/fy-nocase'
        return if ($p/w:r/w:t[contains(text(),' nom.')]) then
            <fr>{normalize-space($fr)}</fr>
        else
            <fr>{lower-case(normalize-space($fr))}</fr>
        ,
        for $fr in local:fr($p[w:r/w:t[local:mayHaveFr(text())]])
        where not(matches($fr,'^[A-Z][a-zA-Z, ûôúâêéëïöó\(\)]*$'))
        return <frerror>{normalize-space($fr)}</frerror>)}</entry>
,
<nonl>{
for $p in $doc/w:document/w:body/w:p[not(w:r/w:rPr/w:u)]
let $h := local:hylperword($p/w:r[string-length(normalize-space(w:t/text())) > 0 and not(w:rPr/w:vanish) and starts-with(w:rPr/w:rStyle/@w:val,'Style')][1])
return if (not(empty($h))) then
    <hylp>{$h}</hylp> else ()
}</nonl>
,
<nofr>{
for $p in $doc/w:document/w:body/w:p[not(w:r/w:t[local:mayHaveFr(text())])]
let $h := local:hylperword($p/w:r[string-length(normalize-space(w:t/text())) > 0 and not(w:rPr/w:vanish) and starts-with(w:rPr/w:rStyle/@w:val,'Style')][1])
return if (not(empty($h))) then
    <hylp>{$h}</hylp> else ()
}</nofr>)}
</index>
