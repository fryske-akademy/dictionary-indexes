xquery version "3.0";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "text";

let $doc := doc("lijst.xml")
return
    for $fr in $doc/index/entry/fr
    let $f := $fr
    group by $fr
    order by $fr collation 'http://fryske-akademy.nl/fy-nocase'
    return ($fr, '  ',
        for $h in distinct-values($doc/index/entry/hylp[../fr/text()[.=$f/text()]]/text())
        return ($h,'; ')
        , '&#13;&#10;')
