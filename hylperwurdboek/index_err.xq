xquery version "3.0";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "text";

let $doc := doc("lijst.xml")
return
    (
        "fouten bij vinden van fries&#13;&#10;&#13;&#10;",
        for $fr in $doc/index/entry/frerror
        let $h := $fr/../hylp
        return ($h, ' met fries?:  ',$fr, '&#13;&#10;'),
        "&#13;&#10;ontbrekend nederlands&#13;&#10;&#13;&#10;",
        for $nl in $doc/index/nonl/hylp
        return ($nl, ' zonder nederlands&#13;&#10;'),
        "&#13;&#10;ontbrekend fries&#13;&#10;&#13;&#10;",
        for $nl in $doc/index/nofr/hylp
        return ($nl, ' zonder fries&#13;&#10;')

    )
